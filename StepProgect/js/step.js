    $(document).ready(function () {
        $(".services-link").click(function () {
            $(this).addClass("active").siblings().removeClass("active").closest(".our-services").find(".services-description").removeClass("active").eq($(this).index()).addClass("active");
        });
//Фильт по категориям
        $(".portfolio-item").slice(12).hide();
        $(".menu-work-link").click(function () {
            $(this).addClass("active").siblings().removeClass("active");
        });

        $(".menu-work-link").click(function () {
            $(".portfolio-item").hide();
//            console.log($(this).attr("data-target"));
            const activeCategory = $(this).attr("data-target");
            if (activeCategory === "all") {
                $(".portfolio-item").slice(0, 12).show();
            }
            else {
                $(".portfolio-item."+ activeCategory).slice(0, 12).show();
            }

        });
        $(".btn-load-more").click(function (e) {
            e.preventDefault();
            const activeCategory = $(".menu-work-link.active").attr("data-target");
            if (activeCategory === "all") {
            $(".portfolio-item:hidden").slice(0, 12).show();
            }
            else {
                $(".portfolio-item."+ activeCategory +":hidden").slice(0, 12).show();
            }
            if (!$(".portfolio-item:hidden").length) {
                $(this).remove();
            }
        });

//       const y = (условия) ? : ;

        //Слайдер с авторами и отзывами
        $(".review-author-mini-foto").click(function () {
            $(this).addClass("active-foto").siblings().removeClass("active-foto").closest(".about-theHam").find(".review-item").removeClass("active").eq($(this).index() - 1).addClass("active");
        });
        $("#btn-slide-prev").click(function (e) {
            e.preventDefault();
            const thumbnails = $(".review-author-mini-foto");
            let needIndex = 0;
            for(let i = 0; ; i++) {
                if($(`.review-author-mini-foto:eq(${i})`).hasClass("active-foto")) {
                    needIndex = i;
                    break;
                }
            }

            $(`.review-author-mini-foto:eq(${needIndex})`).removeClass("active-foto");
            $(`.review-author-mini-foto:eq(${needIndex-1})`).addClass("active-foto");
            $(`.review-item:eq(${needIndex})`).removeClass("active");
            $(`.review-item:eq(${needIndex-1})`).addClass("active");

        });
        $("#btn-slide-next").click(function (e) {
            e.preventDefault();
            e.preventDefault();
            const thumbnails = $(".review-author-mini-foto");
            let needIndex = 0;
            for(let i = 0; ; i++) {
                if($(`.review-author-mini-foto:eq(${i})`).hasClass("active-foto")) {
                    needIndex = i;
                    break;
                }
            }
            $(`.review-author-mini-foto:eq(${needIndex})`).removeClass("active-foto");

            if($(`.review-author-mini-foto:eq(${needIndex+1})`).length != 0) {
                $(`.review-author-mini-foto:eq(${needIndex+1})`).addClass("active-foto");
            }
            else {
                $(`.review-author-mini-foto:eq(0)`).addClass("active-foto");
            }
            $(`.review-item:eq(${needIndex})`).removeClass("active");

            if($(`.review-item:eq(${needIndex+1})`).length != 0) {
                $(`.review-item:eq(${needIndex+1})`).addClass("active");
            }
            else {
                $(`.review-item:eq(0)`).addClass("active");
            }

            $(`.review-item:eq(${needIndex+1})`).addClass("active");

        });
    });
